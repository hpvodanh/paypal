package DuAn2.Controller;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import DuAn2.Paypal.PayPalResult;
import DuAn2.Paypal.PayPalSucess;
import DuAn2.Services.PaypalServices;



@Controller
@RequestMapping("cart")
public class CartController {

	
	@Autowired
	private PaypalServices paypalServices;
	
	@RequestMapping(value = "invoice",method = RequestMethod.GET)
	public String index(ModelMap modelMap) {
		modelMap.put("paypalConfig", paypalServices.gePayPalConfig());
		return "invoice";
	}
	
	
	@RequestMapping(value = "success",method = RequestMethod.GET)
	public String success(HttpServletRequest request, HttpSession session) {
		PayPalSucess payPalSucess = new PayPalSucess();
		PayPalResult payPalResult = payPalSucess.getPayPal(request, paypalServices.gePayPalConfig());
		System.out.println("Transaction Info");
		System.out.println("City " + payPalResult.getAddress_city());
		/*Create new order*/
		/*Create order details*/
		/*Destroy cart*/
		session.removeAttribute("cart");
		return "success";
	}
	
}
