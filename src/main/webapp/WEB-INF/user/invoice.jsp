<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    
    <link
	href="css/style-invoice.css"
	rel="stylesheet">
</head>

<body>

    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
        <s:forEach var="item" items="${sessionScope.cart }">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="hinh/iconhome.ico" width="150" height="150"">
                            </td>
                            
                            <td>
                                Invoice #: ${item.roomCode }<br>
                                Created: ${item.checkInDate }<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Homie Hotel, Inc.<br>
                                Phone: 0977525030<br>
                                Address: 273 Nguyen Gia Tri, Ward 25,<br>
                                 Binh Thanh, City. Ho Chi Minh
                            </td>
                            
                            <td>
                                Dear: ${item.name }.<br>
                                Phone: ${itemphoneNumber }<br>
                                Email: ${item.email }
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    Check #
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    Price Room
                </td>
                
                <td>
                    ${item.roomPrice }
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Check
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Room ID
                </td>
                
                <td>
                    ${item.roomNumber}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Check-in Date
                </td>
                
                <td>
                    ${item.checkInDate}
                </td>
            </tr>
            
            <tr class="item last">
                <td>
                    Check-out Date
                </td>
                
                <td>
                     ${item.checkOutDate}
                </td>
            </tr>
                     
            </s:forEach>
        </table>
    </div>
    <form action="${paypalConfig.posturl }" method="post">
	<!-- PayPal Setting --> 
		<input type="hidden" name="upload" value="1" /> 
		<input type="hidden" name="return" value="${paypalConfig.returnurl }" /> 
		<input type="hidden" name="cmd" value="_cart" /> 
		<input type="hidden" name="business" value="${paypalConfig.business}" />
		
		<s:forEach var="item" items="${sessionScope.cart }">
			<input type="hidden" name="item_number_" value="${item.roomCode }" /> 
			<input type="hidden" name="item_name_" value="${item.name} " /> 
			<input type="hidden" name="amount_" value="${item.roomPrice} " /> 
			<input type="hidden" name="roomNumber_" value="${item.roomNumber}" /> 
		</s:forEach>
		<input type="submit" value="Paypal">
	</form>
</body>
</html>
